use std::convert::*;
use std::fs::*;

#[derive(Clone)]
pub struct ComputerState {
    pub tape: Vec<isize>,
    ip: usize,
    relative_base: isize,
}

#[repr(isize)]
#[derive(PartialEq)]
enum Mode {
    Position = 0,
    Immediate = 1,
    Relative = 2,
}

impl Mode {
    fn from(i: isize) -> Option<Mode> {
        match i {
            0 => Some(Mode::Position),
            1 => Some(Mode::Immediate),
            2 => Some(Mode::Relative),
            _ => None,
        }
    }
}

pub fn read_tape(filename: &str) -> anyhow::Result<ComputerState> {
    Ok(ComputerState {
        tape: read_to_string(filename)?
            .split(",")
            .map(|s| s.trim().parse::<isize>().unwrap())
            .collect::<Vec<_>>(),
        ip: 0,
        relative_base: 0,
    })
}

impl ComputerState {
    fn operand(&mut self) -> isize {
        let val = self.tape[self.ip];
        self.ip += 1;
        val
    }

    fn read(&self, mode: Mode, operand: isize) -> isize {
        let index = match mode {
            Mode::Position => operand as usize,
            Mode::Immediate => return operand,
            Mode::Relative => (self.relative_base + operand) as usize,
        };

        if index >= self.tape.len() {
            0
        } else {
            self.tape[index]
        }
    }

    fn write(&mut self, mode: Mode, operand: isize) -> &mut isize {
        let index = match mode {
            Mode::Position | Mode::Immediate => operand as usize,
            Mode::Relative => (self.relative_base + operand) as usize,
        };

        if index >= self.tape.len() {
            self.tape.resize(index + 1, 0);
        }

        self.tape.get_mut(index).unwrap()
    }

    pub fn eval<T>(&mut self, input: T) -> (Vec<isize>, bool)
    where
        T: IntoIterator,
        T::Item: Into<isize>,
    {
        let mut iterator = input.into_iter();
        let mut output = vec![];

        loop {
            let operator = self.operand();
            let opcode = operator % 100;
            let mode1 = Mode::from((operator / 100) % 10).unwrap();
            let mode2 = Mode::from((operator / 1000) % 10).unwrap();
            let mode3 = Mode::from((operator / 10000) % 10).unwrap();

            match opcode {
                1 => {
                    let operand1 = self.operand();
                    let operand2 = self.operand();
                    let operand3 = self.operand();
                    *self.write(mode3, operand3) =
                        self.read(mode1, operand1) + self.read(mode2, operand2);
                }
                2 => {
                    let operand1 = self.operand();
                    let operand2 = self.operand();
                    let operand3 = self.operand();
                    *self.write(mode3, operand3) =
                        self.read(mode1, operand1) * self.read(mode2, operand2);
                }
                3 => {
                    let operand1 = self.operand();
                    match iterator.next() {
                        Some(val) => *self.write(mode1, operand1) = val.into(),
                        None => {
                            self.ip -= 2;
                            break (output, false);
                        }
                    }
                }
                4 => {
                    let operand1 = self.operand();
                    output.push(self.read(mode1, operand1));
                }
                5 => {
                    let operand1 = self.operand();
                    let operand2 = self.operand();
                    if self.read(mode1, operand1) != 0 {
                        self.ip = self.read(mode2, operand2) as usize;
                    }
                }
                6 => {
                    let operand1 = self.operand();
                    let operand2 = self.operand();
                    if self.read(mode1, operand1) == 0 {
                        self.ip = self.read(mode2, operand2) as usize;
                    }
                }
                7 => {
                    let operand1 = self.operand();
                    let operand2 = self.operand();
                    let operand3 = self.operand();
                    *self.write(mode3, operand3) =
                        (self.read(mode1, operand1) < self.read(mode2, operand2)) as isize;
                }
                8 => {
                    let operand1 = self.operand();
                    let operand2 = self.operand();
                    let operand3 = self.operand();
                    *self.write(mode3, operand3) =
                        (self.read(mode1, operand1) == self.read(mode2, operand2)) as isize;
                }
                9 => {
                    let operand1 = self.operand();
                    self.relative_base += self.read(mode1, operand1);
                }
                99 => break (output, true),
                _ => panic!(),
            }
        }
    }
}
