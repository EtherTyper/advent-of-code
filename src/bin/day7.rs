use scanner_rust::*;
use std::collections::*;
use std::fs::*;
use std::io::*;

fn main() -> anyhow::Result<()> {
    let input = File::open("day7.in")?;
    let lines = BufReader::new(input).lines();

    let mut parents = HashMap::<String, HashSet<String>>::new();
    let mut children = HashMap::<String, HashMap<String, usize>>::new();

    for line in lines {
        let line = line?;
        let mut line_scanner = Scanner::new(line.as_bytes());
        let parent = format!(
            "{} {}",
            line_scanner.next()?.unwrap(),
            line_scanner.next()?.unwrap()
        );
        line_scanner.drop_next()?;
        line_scanner.drop_next()?;
        line_scanner.skip_whitespaces()?;

        let mut children_for_parent = HashMap::<String, usize>::new();

        if &line_scanner.peek(true)?[0..2] != "no".as_bytes() {
            while !line_scanner.peek(true)?.is_empty() {
                let amount = line_scanner.next_usize()?.unwrap();
                let child = format!(
                    "{} {}",
                    line_scanner.next()?.unwrap(),
                    line_scanner.next()?.unwrap()
                );
                line_scanner.drop_next()?;

                if parents.get(&child) == None {
                    parents.insert(child.clone(), HashSet::new());
                }
                parents.get_mut(&child).unwrap().insert(parent.clone());
                children_for_parent.insert(child, amount);
            }
        }

        children.insert(parent, children_for_parent);
    }

    // Part A
    let mut visited = HashSet::<&String>::new();
    let mut queue = LinkedList::<&String>::new();
    let shiny = String::from("shiny gold");
    queue.push_back(&shiny);

    while !queue.is_empty() {
        let top = queue.pop_front().unwrap();
        if visited.contains(&top) {
            continue;
        }

        if let Some(set) = parents.get(top) {
            for parent in set {
                queue.push_back(parent);
            }
        }

        visited.insert(top);
    }

    // Part B
    let mut solved = HashMap::<&String, usize>::new();
    let mut queue = LinkedList::<&String>::new();
    queue.push_back(&shiny);

    while !queue.is_empty() {
        let top = queue.pop_front().unwrap();
        if solved.contains_key(&top) {
            continue;
        }

        let map = children.get(top).unwrap();
        if map.is_empty() {
            solved.insert(top, 1);
            continue;
        }

        if let Some((child, _)) = map.iter().find(|&(child, _)| !solved.contains_key(child)) {
            queue.push_back(child);
            queue.push_back(top);
        } else {
            let mut sum = 1;

            for (child, amount) in map {
                sum += solved[child] * amount;
            }

            solved.insert(top, sum);
        }
    }

    // -1 to filter out shiny gold bag.
    println!("{}", visited.len() - 1);
    println!("{}", solved[&shiny] - 1);

    Ok(())
}
