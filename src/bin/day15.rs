use std::cmp::*;
use std::fs::*;

fn main() -> anyhow::Result<()> {
    let index1 = 2020;
    let index2 = 30000000;

    let mut integers = read_to_string("day15.in")?
        .split(",")
        .map(|s| s.trim().parse::<usize>().unwrap())
        .collect::<Vec<_>>();
    let mut last_spoken = vec![0; index2].into_boxed_slice();

    for (index, &integer) in integers.iter().enumerate() {
        last_spoken[integer] = index + 1;
    }

    for i in integers.len()..max(index1, index2) {
        match last_spoken[integers[i - 1]] {
            0 => integers.push(0),
            time => integers.push(i - time),
        };

        last_spoken[integers[i - 1]] = i;
        if i == index1 - 1 || i == index2 - 1 {
            println!("{}", integers[i]);
        }
    }

    Ok(())
}
