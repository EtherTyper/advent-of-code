use std::collections::*;
use std::fs::*;
use std::io::*;

fn main() -> anyhow::Result<()> {
    let input = File::open("day1.in")?;
    let lines = BufReader::new(input).lines();

    let mut integers = HashSet::<isize>::new();

    for line in lines {
        let val = line?.parse()?;
        integers.insert(val);

        let remaining = 2020 - val;
        if integers.contains(&remaining) {
            println!("{}", val * remaining);
        }
    }

    'loop_a: for a in &integers {
        for b in &integers {
            let remaining = 2020 - a - b;
            if integers.contains(&remaining) {
                println!("{}", a * b * remaining);
                break 'loop_a;
            }
        }
    }

    Ok(())
}
