use aoc::intcode;
use aoc::util::*;
use std::collections::*;
use std::iter::*;

gflags::define! {
    -d, --debug: bool
}

fn main() -> anyhow::Result<()> {
    let initial_state = intcode::read_tape("2019day13.in")?;
    gflags::parse();

    // Part A.
    let mut blocks = HashSet::<Point<isize>>::new();

    let (output, _) = initial_state.clone().eval(empty::<isize>());

    let mut iterator = output.into_iter().peekable();
    while iterator.peek() != None {
        let point = Point {
            x: iterator.next().unwrap(),
            y: iterator.next().unwrap(),
        };
        match iterator.next().unwrap() {
            2 => blocks.insert(point),
            _ => blocks.remove(&point),
        };
    }

    println!("{}", blocks.len());

    // Part B.
    let mut state = initial_state;
    state.tape[0] = 2;
    let mut pixels = HashMap::<Point<isize>, String>::new();
    let mut input_sequence: Vec<isize> = vec![];

    let mut paddle_pos = None;
    let mut ball_pos = None;
    let mut score = None;

    loop {
        let (output, complete) = state.eval(input_sequence);

        // Read in updates
        let mut iterator = output.into_iter().peekable();
        while iterator.peek() != None {
            let (x, y) = (iterator.next().unwrap(), iterator.next().unwrap());
            let pixel_type = iterator.next().unwrap();

            if x == -1 {
                score = Some(pixel_type);
            } else {
                pixels.insert(Point { x, y }, format!("{}", pixel_type));
                match pixel_type {
                    3 => paddle_pos = Some(Point { x, y }),
                    4 => ball_pos = Some(Point { x, y }),
                    _ => continue,
                };
            }
        }

        // Display
        if DEBUG.is_present() {
            print!("\u{001b}[?25l"); // Disable cursor.
            let min_y = pixels.keys().min_by_key(|point| point.y).unwrap().y;
            let max_y = pixels.keys().max_by_key(|point| point.y).unwrap().y;
            let min_x = pixels.keys().min_by_key(|point| point.x).unwrap().x;
            let max_x = pixels.keys().max_by_key(|point| point.x).unwrap().x;
            for y in (min_y..=max_y).rev() {
                for x in min_x..=max_x {
                    // Colored space, depending on type of object.
                    print!(
                        "\u{001b}[4{}m \u{001b}[0m",
                        pixels.get(&Point { x, y }).unwrap_or(&String::from(" "))
                    );
                }
                println!();
            }

            if complete {
                print!("\u{001b}[?25h"); // Enable cursor.
            } else {
                print!("\u{001b}[{}A", max_y - min_y + 1); // Move 22 lines up (size of board.)
            }
        }

        if complete {
            println!("Final score: {}", score.unwrap());
            break;
        }

        // Automatic Input. Follow the ball.
        input_sequence = vec![(ball_pos.unwrap().x - paddle_pos.unwrap().x).signum()];
    }

    Ok(())
}
