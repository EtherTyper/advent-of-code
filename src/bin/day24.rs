use aoc::util::*;
use std::collections::*;
use std::fs::*;
use std::io::*;

fn main() -> anyhow::Result<()> {
    let input = File::open("day24.in")?;
    let lines = BufReader::new(input).lines().map(Result::unwrap);

    let mut map = HashMap::<Point<isize>, bool>::new();

    for line in lines {
        let mut chars = line.chars();
        let mut position = Point { x: 0, y: 0 };

        while let Some(c) = chars.next() {
            position += match c {
                'n' => match chars.next() {
                    Some('w') => Point { x: 0, y: 1 },
                    Some('e') => Point { x: 1, y: 1 },
                    _ => panic!(),
                },
                's' => match chars.next() {
                    Some('w') => Point { x: -1, y: -1 },
                    Some('e') => Point { x: 0, y: -1 },
                    _ => panic!(),
                },
                'w' => Point { x: -1, y: 0 },
                'e' => Point { x: 1, y: 0 },
                _ => panic!(),
            };
        }
        let &current_value = map.get(&position).unwrap_or(&false);
        map.insert(position, !current_value);
    }

    println!("{}", map.iter().filter(|(_, &toggle)| toggle).count());

    // Part B
    let mut min_x = map.keys().min_by_key(|&Point { x, y: _ }| x).unwrap().x;
    let mut max_x = map.keys().max_by_key(|&Point { x, y: _ }| x).unwrap().x;
    let mut min_y = map.keys().min_by_key(|&Point { x: _, y }| y).unwrap().y;
    let mut max_y = map.keys().max_by_key(|&Point { x: _, y }| y).unwrap().y;

    let mut grid = Vec::with_capacity((max_x - min_x + 1) as usize);
    for x in min_x..=max_x {
        grid.push(Vec::with_capacity((max_y - min_y + 1) as usize));
        for y in min_y..=max_y {
            grid[(x - min_x) as usize].push(*map.get(&Point { x, y }).unwrap_or(&false));
        }
    }

    let neighbor_deltas = vec![
        Point { x: 0, y: 1 },
        Point { x: 1, y: 1 },
        Point { x: -1, y: -1 },
        Point { x: 0, y: -1 },
        Point { x: -1, y: 0 },
        Point { x: 1, y: 0 },
    ];

    let steps = 100;

    for _ in 0..steps {
        let old_min_x = min_x;
        let old_max_x = max_x;
        let old_min_y = min_y;
        let old_max_y = max_y;

        min_x -= 1;
        max_x += 1;
        min_y -= 1;
        max_y += 1;

        let mut new_grid = Vec::with_capacity((max_x - min_x + 1) as usize);
        for x in min_x..=max_y {
            new_grid.push(Vec::with_capacity((max_y - min_y + 1) as usize));
            for y in min_y..=max_y {
                let position = Point { x, y };

                let mut neighbor_count = 0;
                for &neighbor_delta in &neighbor_deltas {
                    let neighbor = position + neighbor_delta;
                    if neighbor.x < old_min_x
                        || neighbor.x > old_max_x
                        || neighbor.y < old_min_x
                        || neighbor.y > old_max_y
                    {
                        continue;
                    }
                    let neighbor_value =
                        grid[(neighbor.x - old_min_x) as usize][(neighbor.y - old_min_y) as usize];
                    neighbor_count += neighbor_value as usize;
                }

                new_grid[(position.x - min_x) as usize].push(
                    if position.x < old_min_x
                        || position.x > old_max_x
                        || position.y < old_min_x
                        || position.y > old_max_y
                    {
                        neighbor_count == 2
                    } else {
                        match grid[(position.x - old_min_x) as usize]
                            [(position.y - old_min_y) as usize]
                        {
                            true => neighbor_count == 1 || neighbor_count == 2,
                            false => neighbor_count == 2,
                        }
                    },
                );
            }
        }

        grid = new_grid;
    }

    println!(
        "{}",
        grid.clone()
            .into_iter()
            .map(|vec| vec.into_iter().filter(|&b| b).count())
            .sum::<usize>()
    );

    Ok(())
}
