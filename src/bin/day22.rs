use std::cmp::*;
use std::collections::*;
use std::fs::*;
use std::io::*;
use Winner::*;

fn main() -> anyhow::Result<()> {
    let input = File::open("day22.in")?;
    let mut lines = BufReader::new(input).lines().map(Result::unwrap);

    let mut initial_player1: Vec<usize> = vec![];
    let mut initial_player2: Vec<usize> = vec![];

    lines.next();
    while let Some(line) = lines.next() {
        if line.is_empty() {
            break;
        }
        initial_player1.push(line.parse()?);
    }

    lines.next();
    while let Some(line) = lines.next() {
        if line.is_empty() {
            break;
        }
        initial_player2.push(line.parse()?);
    }

    // Part A
    let mut player1 = initial_player1.clone();
    let mut player2 = initial_player2.clone();

    while !player1.is_empty() && !player2.is_empty() {
        let card1 = player1.remove(0);
        let card2 = player2.remove(0);

        match card1.cmp(&card2) {
            Ordering::Less => {
                player2.push(card2);
                player2.push(card1);
            }
            Ordering::Greater => {
                player1.push(card1);
                player1.push(card2);
            }
            Ordering::Equal => panic!(),
        }
    }

    let mut winner = if player1.is_empty() { player2 } else { player1 };

    let mut i = 0;
    let mut score = 0;
    while let Some(card) = winner.pop() {
        i += 1;
        score += card * i;
    }

    println!("{}", score);

    // Part B
    let mut player1 = initial_player1;
    let mut player2 = initial_player2;
    let mut winner = match recursive_combat(&mut player1, &mut player2) {
        Player1 => player1,
        Player2 => player2,
    };

    let mut i = 0;
    let mut score = 0;
    while let Some(card) = winner.pop() {
        i += 1;
        score += card * i;
    }

    println!("{}", score);

    Ok(())
}

#[derive(Debug)]
enum Winner {
    Player1,
    Player2,
}

fn recursive_combat(player1: &mut Vec<usize>, player2: &mut Vec<usize>) -> Winner {
    let mut history = HashSet::new();

    while !player1.is_empty() && !player2.is_empty() {
        if !history.insert((player1.clone(), player2.clone())) {
            return Player1;
        }

        let card1 = player1.remove(0);
        let card2 = player2.remove(0);

        let round_winner = if player1.len() >= card1 && player2.len() >= card2 {
            let mut copy1 = Vec::from(&player1[0..card1]);
            let mut copy2 = Vec::from(&player2[0..card2]);
            recursive_combat(&mut copy1, &mut copy2)
        } else {
            match card1.cmp(&card2) {
                Ordering::Less => Player2,
                Ordering::Greater => Player1,
                Ordering::Equal => panic!(),
            }
        };

        match round_winner {
            Player1 => {
                player1.push(card1);
                player1.push(card2);
            }
            Player2 => {
                player2.push(card2);
                player2.push(card1);
            }
        }
    }

    if player1.is_empty() {
        Player2
    } else {
        Player1
    }
}
