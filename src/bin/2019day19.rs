use aoc::intcode;
use std::collections::*;

fn main() -> anyhow::Result<()> {
    let initial_state = intcode::read_tape("2019day19.in")?;

    // Part A.
    let radius: isize = 50;
    let mut affected = 0;
    for x in 0..radius {
        for y in 0..radius {
            let (output, _) = initial_state.clone().eval(vec![x, y]);
            affected += output[0];

            // Visualization:
            // if upper_set.get(&x) == Some(&y) {
            //     if lower_set.get(&y) == Some(&x) {
            //         print!("\u{001b}[43m"); // Yellow
            //     } else {
            //         print!("\u{001b}[41m"); // Red
            //     }
            // } else if lower_set.get(&y) == Some(&x) {
            //     print!("\u{001b}[42m"); // Green
            // }
            // print!("{}", output[0]);
            // print!("\u{001b}[0m");
            // if y == radius - 1 {
            //     println!();
            // }
        }
    }
    println!("{}", affected);

    // Part B.
    // Trace out lower half of curve.
    let upper_corner: (isize, isize) = (4, 3); // Upper corner of spire. Discovered through visualization.
    let limit = 2000;

    let mut upper_set = HashMap::new();
    let (mut x, mut y) = upper_corner;
    while x <= limit {
        while initial_state.clone().eval(vec![x, y]).0[0] == 1 {
            y += 1;
        }

        upper_set.insert(x, y - 1);
        x += 1;
    }

    let mut lower_set = HashMap::new();
    let (mut x, mut y) = upper_corner;
    while y <= limit {
        while initial_state.clone().eval(vec![x, y]).0[0] == 1 {
            x += 1;
        }

        lower_set.insert(y, x - 1);
        y += 1;
    }

    // Goal: Find (x_0, y_0) in lower_set, (x_1, y_1) in upper set
    // where x_0 - x_1 >= 100 - 1, y_1 - y_0 >= 100 - 1.
    // The 100x100 rectangle with upper left corner (x_1, y_0) is the first such rectangle.
    let square_size = 100;

    let (mut x_1, mut y_0) = upper_corner;
    loop {
        let x_0 = lower_set.get(&y_0).unwrap();
        let y_1 = upper_set.get(&x_1).unwrap();
        if x_0 - x_1 < square_size - 1 {
            y_0 += 1;
        } else if y_1 - y_0 < square_size - 1 {
            x_1 += 1;
        } else {
            break;
        }
    }
    println!("{}", x_1 * 10000 + y_0);

    Ok(())
}
