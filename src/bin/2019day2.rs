use aoc::intcode;
use std::iter::*;

fn main() -> anyhow::Result<()> {
    let initial_state = intcode::read_tape("2019day2.in")?;
    let tape_length = initial_state.tape.len();

    println!("{}", eval(&mut initial_state.clone(), 12, 2));

    for noun in 0..tape_length {
        for verb in 0..tape_length {
            if eval(&mut initial_state.clone(), noun, verb) == 19690720 {
                println!("{}", 100 * noun + verb);
            }
        }
    }

    Ok(())
}

fn eval(state: &mut intcode::ComputerState, noun: usize, verb: usize) -> usize {
    state.tape[1] = noun as isize;
    state.tape[2] = verb as isize;

    state.eval(empty::<isize>());

    state.tape[0] as usize
}
