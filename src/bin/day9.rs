use std::cmp::*;
use std::collections::*;
use std::fs::*;
use std::io::*;

fn main() -> anyhow::Result<()> {
    let input = File::open("day9.in")?;
    let lines = BufReader::new(input).lines();

    let lookback = 25;

    // Input.
    let mut integers: Vec<usize> = vec![];
    for line in lines {
        let val = line?.parse::<usize>()?;
        integers.push(val);
    }

    // Part A.
    let invalid_index = (lookback..integers.len()).into_iter().find(|&index| {
        let val = integers[index];
        let mut set = HashSet::<usize>::new();

        let pair_exists = integers[index - lookback..].iter().any(|&addend| {
            val >= addend && set.contains(&(val - addend)) || {
                set.insert(addend);
                false
            }
        });

        !pair_exists
    });

    let invalid = integers[invalid_index.unwrap()];
    println!("{}", invalid);

    // Part B.
    let mut lower = 0;
    let mut upper = 0;
    let mut sum = 0;

    loop {
        match sum.cmp(&invalid) {
            Ordering::Less => {
                sum = sum + integers[upper];
                upper = upper + 1;
            }
            Ordering::Greater => {
                sum = sum - integers[lower];
                lower = lower + 1;
            }
            Ordering::Equal => {
                if upper - lower >= 2 {
                    break;
                } else {
                    sum = sum + integers[upper];
                    upper = upper + 1;
                }
            }
        }
    }

    let range_min = integers[lower..upper].iter().min().unwrap();
    let range_max = integers[lower..upper].iter().max().unwrap();
    println!("{}", range_min + range_max);

    Ok(())
}
