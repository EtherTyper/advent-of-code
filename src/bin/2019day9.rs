use aoc::intcode;

fn main() -> anyhow::Result<()> {
    let initial_state = intcode::read_tape("2019day9.in")?;

    let (output, _) = initial_state.clone().eval(vec![1 as isize]);
    println!("{}", output[0]);

    let mut state = initial_state;
    let (output, _) = state.eval(vec![2 as isize]);
    println!("{}", output[0]);

    Ok(())
}
