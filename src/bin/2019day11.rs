use aoc::intcode;
use aoc::util::*;
use std::collections::*;

fn main() -> anyhow::Result<()> {
    let initial_state = intcode::read_tape("2019day11.in")?;

    // Part A.
    let mut painted = HashMap::<Point<isize>, bool>::new();

    eval(&mut initial_state.clone(), &mut painted);
    println!("{}", painted.len());

    // Part B.
    let mut painted = HashMap::<Point<isize>, bool>::new();
    painted.insert(Point { x: 0, y: 0 }, true);

    eval(&mut initial_state.clone(), &mut painted);
    let min_y = painted.keys().min_by_key(|point| point.y).unwrap().y;
    let max_y = painted.keys().max_by_key(|point| point.y).unwrap().y;
    let min_x = painted.keys().min_by_key(|point| point.x).unwrap().x;
    let max_x = painted.keys().max_by_key(|point| point.x).unwrap().x;
    for y in (min_y..=max_y).rev() {
        for x in min_x..=max_x {
            print!(
                "{}",
                if *painted.get(&Point { x, y }).unwrap_or(&false) {
                    "*"
                } else {
                    " "
                }
            );
        }
        println!();
    }

    Ok(())
}

fn eval(state: &mut intcode::ComputerState, painted: &mut HashMap<Point<isize>, bool>) {
    let mut position = Point { x: 0, y: 0 };
    let mut facing = 0;

    loop {
        let &current_painted = painted.get(&position).unwrap_or(&false);
        let (output, complete) = state.eval(vec![current_painted as isize]);

        painted.insert(position, output[0] != 0);

        facing = modulo(facing + 2 * output[1] - 1, 4);
        position += match facing {
            0 => Point { x: 0, y: 1 },
            1 => Point { x: 1, y: 0 },
            2 => Point { x: 0, y: -1 },
            3 => Point { x: -1, y: 0 },
            _ => panic!(),
        };

        if complete {
            break;
        }
    }
}
