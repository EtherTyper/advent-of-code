use aoc::intcode;
use std::collections::*;

fn main() -> anyhow::Result<()> {
    let directions: Vec<(isize, _)> = vec![(1, (0, 1)), (2, (0, -1)), (3, (-1, 0)), (4, (1, 0))];
    let initial_state = intcode::read_tape("2019day15.in")?;

    // Part A.
    let mut distances = HashMap::new();
    let mut queue = LinkedList::new();

    let mut oxygen = None;
    let mut oxygen_state = None;

    distances.insert((0, 0), 0);
    queue.push_back(((0, 0), 0, initial_state));

    propagate_distances(
        &mut distances,
        &mut queue,
        &directions,
        &mut oxygen,
        &mut oxygen_state,
    );

    println!("{}", distances[&oxygen.unwrap()]);

    // Part B.
    let mut distances = HashMap::new();
    let mut queue = LinkedList::new();
    distances.insert(oxygen.unwrap(), 0);
    queue.push_back((oxygen.unwrap(), 0, oxygen_state.unwrap()));

    propagate_distances(
        &mut distances,
        &mut queue,
        &directions,
        &mut None,
        &mut None,
    );

    println!("{}", distances.values().max().unwrap());

    Ok(())
}

fn propagate_distances(
    distances: &mut HashMap<(isize, isize), usize>,
    queue: &mut LinkedList<((isize, isize), usize, intcode::ComputerState)>,
    directions: &Vec<(isize, (isize, isize))>,
    oxygen: &mut Option<(isize, isize)>,
    oxygen_state: &mut Option<intcode::ComputerState>,
) {
    while let Some((point, distance, state)) = queue.pop_front() {
        if distances.contains_key(&point) && distances[&point] < distance {
            continue;
        }

        for &(direction_id, direction_offset) in directions {
            let new_point = (point.0 + direction_offset.0, point.1 + direction_offset.1);
            if distances.contains_key(&new_point) && distances[&new_point] <= distance + 1 {
                continue;
            }

            let mut new_state = state.clone();
            let (output, _) = new_state.eval(vec![direction_id]);
            if output[0] == 0 {
                continue;
            }

            if output[0] == 2 {
                *oxygen = Some(new_point);
                *oxygen_state = Some(new_state.clone());
            }

            distances.insert(new_point, distance + 1);
            queue.push_back((new_point, distance + 1, new_state));
        }
    }
}
