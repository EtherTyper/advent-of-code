use std::fs::*;
use std::io::*;

fn modpow(mut base: usize, mut exp: usize, modulus: usize) -> usize {
    let mut value = 1;
    while exp != 0 {
        if exp & 1 != 0 {
            value = (value * base) % modulus;
        }

        base = (base * base) % modulus;
        exp >>= 1;
    }

    value
}

fn main() -> anyhow::Result<()> {
    let input = File::open("day25.in")?;
    let mut lines = BufReader::new(input).lines().map(Result::unwrap);

    let alice_public: usize = lines.next().unwrap().parse()?;
    let bob_public: usize = lines.next().unwrap().parse()?;

    let base = 7;
    let modulus = 20201227;

    let mut resulting_public = 1;
    for alice_private in 0.. {
        resulting_public = (resulting_public * base) % modulus;
        if resulting_public == alice_public {
            println!("{}", modpow(bob_public, alice_private, modulus));
            break;
        }
    }

    Ok(())
}
