use std::fs::*;
use std::io::*;
use std::iter::*;

fn main() -> anyhow::Result<()> {
    let input = File::open("day18.in")?;
    let lines = BufReader::new(input).lines().map(Result::unwrap);
    let sum = lines
        .map(|line| eval(&mut line.chars().peekable()))
        .sum::<usize>();

    let input = File::open("day18.in")?;
    let lines = BufReader::new(input).lines().map(Result::unwrap);
    let sum2 = lines
        .map(|line| eval_mul(&mut line.chars().peekable()))
        .sum::<usize>();

    println!("{:?}", sum);
    println!("{:?}", sum2);
    Ok(())
}

// Part A.
fn eval<T>(it: &mut Peekable<T>) -> usize
where
    T: Iterator<Item = char>,
{
    let mut accumulator = eval_term(it, eval);

    while it.peek() != None && it.peek() != Some(&')') {
        let op = it.next().unwrap(); // Swallow op.
        it.next(); // Swallow space.
        let next_term = eval_term(it, eval);

        accumulator = match op {
            '+' => accumulator + next_term,
            '*' => accumulator * next_term,
            _ => panic!(),
        };
    }

    accumulator
}

// Part B.
fn eval_mul<T>(it: &mut Peekable<T>) -> usize
where
    T: Iterator<Item = char>,
{
    let mut accumulator = eval_plus(it);

    while it.peek() == Some(&'*') {
        it.next(); // Swallow op.
        it.next(); // Swallow space.
        let next_term = eval_plus(it);

        accumulator *= next_term;
    }

    accumulator
}

fn eval_plus<T>(it: &mut Peekable<T>) -> usize
where
    T: Iterator<Item = char>,
{
    let mut accumulator = eval_term(it, eval_mul);

    while it.peek() == Some(&'+') {
        it.next(); // Swallow op.
        it.next(); // Swallow space.
        let next_term = eval_term(it, eval_mul);

        accumulator += next_term;
    }

    accumulator
}

// Shared.
fn eval_term<T>(it: &mut Peekable<T>, eval: fn(&mut Peekable<T>) -> usize) -> usize
where
    T: Iterator<Item = char>,
{
    let val = match it.next() {
        Some('(') => {
            let val = eval(it);
            it.next(); // Swallow ')'.
            val
        }
        Some(digit) => digit as usize - '0' as usize,
        _ => panic!(),
    };

    while it.peek() == Some(&' ') {
        it.next(); // Swallow spaces.
    }

    val
}
