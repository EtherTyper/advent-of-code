use scanner_rust::*;

// Same as expect, as I now know.
pub fn unwrap_or_panic<T>(obj: Option<T>, message: &'static str) -> anyhow::Result<T> {
    match obj {
        Some(val) => Ok(val),
        None => panic!(message),
    }
}

fn main() -> anyhow::Result<()> {
    let mut scanner = Scanner::scan_path("day2.in")?;
    let mut count = 0;
    let mut count2 = 0;

    while !scanner.peek(true)?.is_empty() {
        let low = scanner.next_usize_until("-")?.unwrap();
        let high = scanner.next_usize()?.unwrap();
        scanner.skip_whitespaces()?;
        let given_letter = scanner.next_char()?.unwrap();
        scanner.next_char()?.unwrap();
        scanner.skip_whitespaces()?;
        let password = scanner.next_line()?.unwrap();

        let mut incidence = 0;
        for character in password.chars() {
            incidence += (character == given_letter) as usize;
        }
        count += (low..=high).contains(&incidence) as usize;

        let low_correct =
            low <= password.len() && password[(low - 1)..low] == given_letter.to_string();
        let high_correct =
            high <= password.len() && password[(high - 1)..high] == given_letter.to_string();

        count2 += (low_correct ^ high_correct) as usize;
    }

    println!("{}", count);
    println!("{}", count2);

    Ok(())
}
