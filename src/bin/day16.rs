use scanner_rust::*;
use std::collections::*;
use std::fs::*;
use std::io::*;

fn main() -> anyhow::Result<()> {
    let input = File::open("day16.in")?;
    let mut lines = BufReader::new(input).lines();

    let mut ranges = vec![];
    let mut options = HashMap::<String, HashSet<usize>>::new();

    let mut num_fields = 0;

    while let Some(line) = lines.next() {
        let line = line?;

        if line.is_empty() {
            break;
        }

        let mut scanner = ScannerStr::new(&line);
        let field_name = String::from(scanner.next_until(":")?.unwrap());
        scanner.skip_whitespaces()?;
        let range_one_start = scanner.next_usize_until("-")?.unwrap();
        let range_one_end = scanner.next_usize()?.unwrap();
        scanner.next()?.unwrap();
        scanner.skip_whitespaces()?;
        let range_two_start = scanner.next_usize_until("-")?.unwrap();
        let range_two_end = scanner.next_usize()?.unwrap();

        options.insert(field_name.clone(), HashSet::new());
        ranges.push((
            field_name,
            range_one_start..=range_one_end,
            range_two_start..=range_two_end,
        ));

        num_fields += 1;
    }

    for (_, possible_indices) in &mut options {
        possible_indices.extend(0..num_fields);
    }

    lines.next();
    let my_ticket = lines
        .next()
        .unwrap()?
        .split(",")
        .map(|s| s.trim().parse::<usize>().unwrap())
        .collect::<Vec<_>>();
    for _ in 0..2 {
        lines.next();
    }

    let mut error_rate = 0;

    while let Some(line) = lines.next() {
        let line = line?;
        let values = line
            .split(",")
            .map(|s| s.trim().parse::<usize>().unwrap())
            .collect::<Vec<_>>();

        for (index, value) in values.into_iter().enumerate() {
            let mut valid = false;

            for (_, range1, range2) in &ranges {
                if range1.contains(&value) || range2.contains(&value) {
                    valid = true;
                }
            }

            if !valid {
                error_rate += value;
            } else {
                for (field_name, range1, range2) in &ranges {
                    if !range1.contains(&value) && !range2.contains(&value) {
                        options.get_mut(field_name).unwrap().remove(&index);
                    }
                }
            }
        }
    }

    println!("{}", error_rate);

    let mut product = 1;

    loop {
        let mut found_pair = None;

        for (field_name, possible_indices) in &options {
            if possible_indices.len() == 1 {
                found_pair = Some((field_name.clone(), *possible_indices.iter().next().unwrap()));
                break;
            }
        }

        if let Some((field_name, index)) = found_pair {
            if field_name.len() >= 9 && &field_name[0..9] == "departure" {
                product *= my_ticket[index];
            }

            for (_, possible_indices) in &mut options {
                possible_indices.remove(&index);
            }
        } else {
            break;
        }
    }

    println!("{}", product);

    Ok(())
}
