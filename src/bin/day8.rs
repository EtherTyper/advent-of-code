use std::collections::*;
use std::fs::*;
use std::io::*;
use std::mem::*;

#[derive(PartialEq)]
enum Opcode {
    Acc,
    Jmp,
    Nop,
}

struct Instruction {
    opcode: Opcode,
    operand: isize,
}

fn main() -> anyhow::Result<()> {
    let input = File::open("day8.in")?;
    let lines = BufReader::new(input).lines();

    let mut instructions: Vec<Instruction> = vec![];

    for line in lines {
        let line = String::from(line?);
        instructions.push(Instruction {
            opcode: match &line[0..3] {
                "acc" => Opcode::Acc,
                "jmp" => Opcode::Jmp,
                "nop" => Opcode::Nop,
                _ => panic!(),
            },
            operand: line[4..].parse()?,
        });
    }

    println!("{}", eval(&instructions).0);
    for i in 0..instructions.len() {
        let mut new_opcode = match instructions[i].opcode {
            Opcode::Acc => continue,
            Opcode::Jmp => Opcode::Nop,
            Opcode::Nop => Opcode::Jmp,
        };

        swap(&mut instructions[i].opcode, &mut new_opcode);
        let (accumulator, terminated) = eval(&instructions);
        swap(&mut instructions[i].opcode, &mut new_opcode);

        if terminated {
            println!("{}", accumulator);
            break;
        }
    }

    Ok(())
}

fn eval(instructions: &Vec<Instruction>) -> (isize, bool) {
    let mut visited_instructions: HashSet<usize> = HashSet::new();

    let mut accumulator = 0;
    let mut ip: usize = 0;

    loop {
        if ip == instructions.len() {
            break (accumulator, true);
        } else if !(0..instructions.len()).contains(&ip) || !visited_instructions.insert(ip) {
            break (accumulator, false);
        }

        if instructions[ip].opcode == Opcode::Acc {
            accumulator += instructions[ip].operand;
        }

        ip = match instructions[ip].opcode {
            Opcode::Jmp => ((ip as isize) + instructions[ip].operand) as usize,
            _ => ip + 1,
        };
    }
}
