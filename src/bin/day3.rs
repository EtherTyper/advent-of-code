use std::fs::*;
use std::io::*;
use std::ops::*;

fn main() -> anyhow::Result<()> {
    let input = File::open("day3.in")?;
    let lines: Vec<_> = BufReader::new(input).lines().map(Result::unwrap).collect();

    println!("{}", answer(&lines, 3, 1));
    println!(
        "{}",
        [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
            .iter()
            .map(|&(x, y)| answer(&lines, x, y))
            .fold(1, Mul::mul)
    );
    Ok(())
}

fn answer(lines: &Vec<String>, right: usize, down: usize) -> usize {
    let mut x = 0;
    let mut y = 0;
    let mut trees = 0;

    while y + down < lines.len() {
        y += down;
        x = (x + right) % lines[y].len();

        if &lines[y][x..=x] == "#" {
            trees += 1;
        }
    }

    trees
}
