use std::collections::*;
use std::fs::*;
use std::io::*;

fn main() -> anyhow::Result<()> {
    let input = File::open("day21.in")?;
    let lines = BufReader::new(input).lines().map(Result::unwrap);

    let mut possible_ingredients = HashMap::<String, HashSet<String>>::new();
    let mut ingredient_incidence = HashMap::<String, usize>::new();

    for line in lines {
        let paren = line.find('(').unwrap();
        let ingredients: HashSet<_> = line[0..paren - 1].split(" ").map(String::from).collect();
        let allergens: Vec<_> = line[paren + 10..line.len() - 1]
            .split(", ")
            .map(String::from)
            .collect();

        for ingredient in &ingredients {
            match ingredient_incidence.get_mut(ingredient) {
                Some(incidence) => *incidence += 1,
                None => {
                    ingredient_incidence.insert(ingredient.to_owned(), 1);
                }
            };
        }

        for allergen in allergens {
            let ingredients = match possible_ingredients.get(&allergen) {
                Some(set) => set.intersection(&ingredients).map(String::from).collect(),
                None => ingredients.clone(),
            };
            possible_ingredients.insert(allergen, ingredients);
        }
    }

    let possible_ingredients_set: HashSet<_> = possible_ingredients
        .iter()
        .flat_map(|(_, ingredients)| ingredients.clone())
        .collect();

    println!(
        "{:?}",
        ingredient_incidence
            .iter()
            .filter(|&(ingredient, _)| !possible_ingredients_set.contains(ingredient))
            .map(|(_, frequency)| frequency)
            .sum::<usize>()
    );

    // Part B.
    let mut unknown_allergens = possible_ingredients;
    let mut known_allergens = BTreeMap::<String, String>::new();

    loop {
        let mut found_allergen = None;

        for (allergen, ingredients) in &unknown_allergens {
            if ingredients.len() == 1 {
                found_allergen =
                    Some((allergen.clone(), ingredients.iter().next().unwrap().clone()));
                break;
            }
        }

        if let Some((allergen, ingredient)) = found_allergen {
            for (_, ingredients) in &mut unknown_allergens {
                ingredients.remove(&ingredient);
            }
            known_allergens.insert(allergen, ingredient);
        } else {
            break;
        }
    }

    println!(
        "{}",
        known_allergens
            .into_iter()
            .map(|(_, ingredient)| ingredient)
            .collect::<Vec<_>>()
            .join(",")
    );

    Ok(())
}
