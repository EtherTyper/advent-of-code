use std::collections::*;
use std::fs::*;
use std::io::*;

fn main() -> anyhow::Result<()> {
    let input = File::open("day20.in")?;
    let mut lines = BufReader::new(input).lines().map(Result::unwrap);

    // Input handling.
    let mut tiles = HashMap::<usize, Vec<Vec<char>>>::new();

    while let Some(id_line) = lines.next() {
        let id = id_line[5..id_line.len() - 1].parse::<usize>()?;

        let mut grid: Vec<Vec<char>> = vec![];

        while let Some(line) = lines.next() {
            if line.is_empty() {
                break;
            }

            grid.push(line.chars().collect());
        }

        tiles.insert(id, grid);
    }

    // Part A.
    let mut picture = HashMap::<(isize, isize), (usize, Vec<Vec<char>>)>::new();
    let mut queue = LinkedList::<(isize, isize)>::new();

    let &initial_id = tiles.iter().next().unwrap().0;

    picture.insert((0, 0), (initial_id, tiles.remove(&initial_id).unwrap()));
    queue.push_back((0, 0));

    while let Some(position) = queue.pop_front() {
        let mut to_remove = vec![];

        for (&other_id, other_grid) in &tiles {
            let mut rotated_other_grid = other_grid.clone();
            'rotations: for _ in 0..=1 {
                for _ in 0..4 {
                    if let Some(shift) = fits(&picture[&position].1, &rotated_other_grid) {
                        let new_position = (position.0 + shift.0, position.1 + shift.1);
                        if !picture.contains_key(&new_position) {
                            to_remove.push(other_id);
                            picture.insert(new_position, (other_id, rotated_other_grid));
                            queue.push_back(new_position);
                            break 'rotations;
                        }
                    }
                    rotated_other_grid = rotate_right(rotated_other_grid);
                }
                rotated_other_grid = flip(rotated_other_grid);
            }
        }

        for id in to_remove {
            tiles.remove(&id);
        }
    }

    let (&(min_x, _), _) = picture.iter().min_by_key(|(&(x, _), _)| x).unwrap();
    let (&(max_x, _), _) = picture.iter().max_by_key(|(&(x, _), _)| x).unwrap();
    let (&(_, min_y), _) = picture.iter().min_by_key(|(&(_, y), _)| y).unwrap();
    let (&(_, max_y), _) = picture.iter().max_by_key(|(&(_, y), _)| y).unwrap();

    println!(
        "{}",
        picture[&(min_x, min_y)].0
            * picture[&(min_x, max_y)].0
            * picture[&(max_x, min_y)].0
            * picture[&(max_x, max_y)].0
    );

    // Part B
    let sea_monster: Vec<Vec<_>> = BufReader::new(File::open("day20seamonster.in")?)
        .lines()
        .map(Result::unwrap)
        .filter(|s| !s.is_empty())
        .map(|s| s.chars().collect())
        .collect();

    let tile_size = (picture[&(0, 0)].1.len(), picture[&(0, 0)].1[0].len());
    let assembled_size = (
        (max_x - min_x + 1) as usize * (tile_size.0 - 2),
        (max_y - min_y + 1) as usize * (tile_size.1 - 2),
    );

    let mut assembled_picture = Vec::with_capacity(assembled_size.0);
    let mut sea_monster_points = Vec::with_capacity(assembled_size.0);
    let mut x = 0;
    for grid_x in min_x..=max_x {
        for tile_x in 1..tile_size.0 - 1 {
            sea_monster_points.push(vec![false; assembled_size.1]);
            assembled_picture.push(Vec::with_capacity(assembled_size.1));
            for grid_y in min_y..=max_y {
                for tile_y in 1..tile_size.1 - 1 {
                    assembled_picture[x].push(picture[&(grid_x, grid_y)].1[tile_x][tile_y]);
                }
            }
            x += 1;
        }
    }

    for _ in 0..=1 {
        for _ in 0..4 {
            for x in 0..assembled_picture.len() - sea_monster.len() + 1 {
                for y in 0..assembled_picture[0].len() - sea_monster[0].len() + 1 {
                    let mut is_monster = true;
                    'check_sea_monster: for delta_x in 0..sea_monster.len() {
                        for delta_y in 0..sea_monster[0].len() {
                            if sea_monster[delta_x][delta_y] == '#'
                                && assembled_picture[x + delta_x][y + delta_y] != '#'
                            {
                                is_monster = false;
                                break 'check_sea_monster;
                            }
                        }
                    }

                    if is_monster {
                        for delta_x in 0..sea_monster.len() {
                            for delta_y in 0..sea_monster[0].len() {
                                if sea_monster[delta_x][delta_y] == '#' {
                                    sea_monster_points[x + delta_x][y + delta_y] = true;
                                }
                            }
                        }
                    }
                }
            }
            sea_monster_points = rotate_right(sea_monster_points);
            assembled_picture = rotate_right(assembled_picture);
        }
        sea_monster_points = flip(sea_monster_points);
        assembled_picture = flip(assembled_picture);
    }

    let mut non_sea_monster_points = 0;
    for x in 0..assembled_picture.len() {
        for y in 0..assembled_picture[0].len() {
            non_sea_monster_points +=
                (assembled_picture[x][y] == '#' && !sea_monster_points[x][y]) as usize;
        }
    }

    println!("{}", non_sea_monster_points);

    Ok(())
}

fn fits(grid1: &Vec<Vec<char>>, grid2: &Vec<Vec<char>>) -> Option<(isize, isize)> {
    if grid1.first() == grid2.last() {
        Some((-1, 0))
    } else if grid1.last() == grid2.first() {
        Some((1, 0))
    } else {
        let mut on_left = true;
        let mut on_right = true;

        for x in 0..grid1.len() {
            on_left &= grid1[x].first() == grid2[x].last();
            on_right &= grid1[x].last() == grid2[x].first();
        }

        if on_left {
            Some((0, -1))
        } else if on_right {
            Some((0, 1))
        } else {
            None
        }
    }
}

fn flip<T>(grid: Vec<Vec<T>>) -> Vec<Vec<T>>
where
    T: Copy,
{
    let original_size = (grid.len(), grid[0].len());

    let mut new_grid: Vec<Vec<T>> = Vec::with_capacity(original_size.0);
    for x in 0..original_size.0 {
        new_grid.push(Vec::with_capacity(original_size.1));
        for y in 0..original_size.1 {
            new_grid[x].push(grid[original_size.0 - x - 1][y]);
        }
    }

    new_grid
}

fn rotate_right<T>(grid: Vec<Vec<T>>) -> Vec<Vec<T>>
where
    T: Copy,
{
    let original_size = (grid.len(), grid[0].len());

    let mut new_grid: Vec<Vec<T>> = Vec::with_capacity(original_size.1);
    for x in 0..original_size.1 {
        new_grid.push(Vec::with_capacity(original_size.0));
        for y in 0..original_size.0 {
            new_grid[x].push(grid[original_size.1 - y - 1][x]);
        }
    }

    new_grid
}
