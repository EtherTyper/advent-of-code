use std::collections::*;
use std::fs::*;
use std::io::*;

fn main() -> anyhow::Result<()> {
    let input = File::open("day5.in")?;
    let ids: BTreeSet<_> = BufReader::new(input)
        .lines()
        .map(|line| id(&line.unwrap()))
        .collect();

    let &max_id = ids.iter().max().unwrap();
    let &min_id = ids.iter().min().unwrap();

    let missing = (min_id..max_id).find(|i| !ids.contains(&i)).unwrap();

    println!("{}", max_id);
    println!("{}", missing);
    Ok(())
}

fn id(line: &String) -> usize {
    let mut id = 0;

    for c in line.chars() {
        id <<= 1;
        id += match c {
            'F' => 0,
            'L' => 0,
            'B' => 1,
            'R' => 1,
            _ => panic!(),
        };
    }

    id
}
