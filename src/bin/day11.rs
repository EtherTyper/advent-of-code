use aoc::util::*;
use std::cmp::*;
use std::fs::*;
use std::io::*;

fn main() -> anyhow::Result<()> {
    let input = File::open("day11.in")?;
    let initial_grid: Vec<Vec<char>> = BufReader::new(input)
        .lines()
        .map(|s| s.unwrap().chars().collect())
        .collect();

    // Part A.
    automoton(&initial_grid, |preimage, (row, col)| {
        let mut neighbors_occupied = 0;
        for neighbor_row in row.checked_sub(1).unwrap_or(0)..min(row + 2, preimage.len()) {
            for neighbor_col in col.checked_sub(1).unwrap_or(0)..min(col + 2, preimage[row].len()) {
                if (neighbor_row, neighbor_col) == (row, col) {
                    continue;
                }
                neighbors_occupied += (preimage[neighbor_row][neighbor_col] == '#') as usize;
            }
        }

        match preimage[row][col] {
            'L' => {
                if neighbors_occupied == 0 {
                    '#'
                } else {
                    'L'
                }
            }
            '#' => {
                if neighbors_occupied >= 4 {
                    'L'
                } else {
                    '#'
                }
            }
            '.' => '.',
            _ => panic!(),
        }
    });

    // Part B.
    automoton(&initial_grid, |preimage, (row, col)| {
        let mut neighbors_occupied = 0;
        for direction_x in -1..=1 {
            for direction_y in -1..=1 {
                if (direction_x, direction_y) == (0, 0) {
                    continue;
                }
                let mut current_pos = Point {
                    x: row as isize + direction_x,
                    y: col as isize + direction_y,
                };
                while (0..preimage.len() as isize).contains(&current_pos.x)
                    && (0..preimage[current_pos.x as usize].len() as isize).contains(&current_pos.y)
                {
                    let neighbor = preimage[current_pos.x as usize][current_pos.y as usize];
                    if neighbor == '.' {
                        current_pos += Point {
                            x: direction_x,
                            y: direction_y,
                        };
                    } else {
                        neighbors_occupied += match neighbor {
                            '#' => 1,
                            'L' => 0,
                            _ => panic!(),
                        };
                        break;
                    }
                }
            }
        }

        match preimage[row][col] {
            'L' => {
                if neighbors_occupied == 0 {
                    '#'
                } else {
                    'L'
                }
            }
            '#' => {
                if neighbors_occupied >= 5 {
                    'L'
                } else {
                    '#'
                }
            }
            '.' => '.',
            _ => panic!(),
        }
    });

    Ok(())
}

fn automoton<K>(initial_grid: &Vec<Vec<char>>, closure: K)
where
    K: Fn(&Vec<Vec<char>>, (usize, usize)) -> char,
{
    let mut preimage = initial_grid.clone();
    let mut image: Vec<Vec<char>>;

    loop {
        image = Vec::with_capacity(preimage.len());

        for row in 0..preimage.len() {
            image.push(Vec::with_capacity(preimage[row].len()));
            for col in 0..preimage[row].len() {
                image[row].push(closure(&preimage, (row, col)));
            }
        }

        if preimage == image {
            break;
        }

        preimage = image;
    }

    println!(
        "{}",
        image
            .into_iter()
            .map(|v| v.into_iter().filter(|&x| x == '#').count())
            .sum::<usize>(),
    );
}
