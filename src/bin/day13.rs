use aoc::util::*;
use std::fs::*;
use std::io::*;

fn ceil_div(a: usize, b: usize) -> usize {
    return (a + (b - 1)) / b;
}

fn extended_gcd(mut a: i128, mut b: i128) -> (i128, Point<i128>) {
    let mut a_coefs = Point { x: 1, y: 0 };
    let mut b_coefs = Point { x: 0, y: 1 };

    while b != 0 {
        let new_a = b;
        let new_b = a % b;

        let new_a_coefs = b_coefs;
        let new_b_coefs = a_coefs - b_coefs * (a / b);

        a = new_a;
        b = new_b;

        a_coefs = new_a_coefs;
        b_coefs = new_b_coefs;
    }

    (a, a_coefs)
}

fn main() -> anyhow::Result<()> {
    let input = File::open("day13.in")?;
    let mut lines = BufReader::new(input).lines();
    let earliest = lines.next().unwrap().unwrap().parse::<usize>().unwrap();
    let bus_frequencies = lines.next().unwrap().unwrap();
    let bus_frequencies: Vec<_> = bus_frequencies
        .split(",")
        .enumerate()
        .flat_map(|(index, s)| match s {
            "x" => vec![],
            _ => vec![(index, s.trim().parse::<usize>().unwrap())],
        })
        .collect();

    let (bus_id, departure_time) = bus_frequencies
        .iter()
        .map(|&(_, x)| (x, ceil_div(earliest, x) * x))
        .min_by_key(|&(_, time)| time)
        .unwrap();

    println!("{}", bus_id * (departure_time - earliest));

    // Solve system x = -bus_id (mod bus_frequency)
    let (earliest_time, _) =
        bus_frequencies
            .into_iter()
            .fold((0, 1), |(rem1, mod1), (rem2, mod2)| {
                let (rem2, mod2) = (rem2 as i128, mod2 as i128);
                let (gcd, Point { x: m, y: _ }) = extended_gcd(mod1, mod2);
                let x = rem1 + (-rem2 - rem1) * m * mod1;
                let new_mod = mod1 * mod2 / gcd;
                (modulo(x, new_mod), new_mod)
            });
    println!("{}", earliest_time);

    Ok(())
}
