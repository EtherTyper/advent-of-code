use aoc::util::*;
use std::fs::*;
use std::io::*;

fn main() -> anyhow::Result<()> {
    let input = File::open("day12.in")?;
    let commands: Vec<(char, isize)> = BufReader::new(input)
        .lines()
        .map(|line| {
            let line = String::from(line.unwrap());

            let command = line.as_bytes()[0] as char;
            let param = line[1..].parse::<isize>().unwrap();

            (command, param)
        })
        .collect();

    // Part A.
    let mut position = Point { x: 0, y: 0 };
    let mut facing = 0;

    for &(command, param) in &commands {
        facing += match command {
            'L' => -param,
            'R' => param,
            _ => 0,
        };
        position += match command {
            'N' => Point { x: 0, y: param },
            'S' => Point { x: 0, y: -param },
            'E' => Point { x: param, y: 0 },
            'W' => Point { x: -param, y: 0 },
            'F' => match modulo(facing, 360) {
                0 => Point { x: param, y: 0 },
                90 => Point { x: 0, y: -param },
                180 => Point { x: -param, y: 0 },
                270 => Point { x: 0, y: param },
                _ => panic!(),
            },
            _ => continue,
        };
    }

    println!("{}", position.x.abs() + position.y.abs());

    // Part B.
    let mut position = Point { x: 0, y: 0 };
    let mut waypoint = Point { x: 10, y: 1 };

    for (command, param) in commands {
        waypoint += match command {
            'N' => Point { x: 0, y: param },
            'S' => Point { x: 0, y: -param },
            'E' => Point { x: param, y: 0 },
            'W' => Point { x: -param, y: 0 },
            _ => Point { x: 0, y: 0 },
        };

        if command == 'L' || command == 'R' {
            let angle = match command {
                'L' => 360 - param,
                'R' => param,
                _ => panic!(),
            };
            waypoint = match angle {
                0 => waypoint,
                90 => Point {
                    x: waypoint.y,
                    y: -waypoint.x,
                },
                180 => -waypoint,
                270 => Point {
                    x: -waypoint.y,
                    y: waypoint.x,
                },
                _ => panic!(),
            };
        }

        if command == 'F' {
            position += waypoint * param;
        }
    }

    println!("{}", position.x.abs() + position.y.abs());

    Ok(())
}
