use scanner_rust::*;
use std::collections::*;
use std::fs::*;
use std::io::*;
use RuleOutput::*;

#[derive(Debug, Clone)]
enum RuleOutput {
    NonTerminal(Vec<Vec<usize>>),
    Terminal(char),
}

fn main() -> anyhow::Result<()> {
    let input = File::open("day19.in")?;
    let mut lines = BufReader::new(input).lines().map(Result::unwrap);
    let mut productions = HashMap::new();

    while let Some(line) = lines.next() {
        if line.is_empty() {
            break;
        }

        let mut scanner = ScannerStr::new(&line);
        let rule_number = scanner.next_until(":")?.unwrap();
        scanner.next_char()?.unwrap();
        scanner.skip_whitespaces()?;

        let rule_first_term = scanner.next()?.unwrap();

        let rule_output = if rule_first_term.as_bytes()[0] == '\"' as u8 {
            Terminal(rule_first_term.as_bytes()[1] as char)
        } else {
            let mut union_out = vec![];
            let mut product_out = vec![];

            product_out.push(rule_first_term.parse()?);

            while let Some(val) = scanner.next()? {
                match val {
                    "|" => {
                        union_out.push(product_out);
                        product_out = vec![];
                    }
                    string => {
                        product_out.push(string.parse()?);
                    }
                }
            }
            union_out.push(product_out);

            NonTerminal(union_out)
        };

        productions.insert(rule_number.parse::<usize>()?, rule_output);
    }

    let mut productions2 = productions.clone();
    productions2.insert(8, NonTerminal(vec![vec![42], vec![42, 8]]));
    productions2.insert(11, NonTerminal(vec![vec![42, 31], vec![42, 11, 31]]));

    let mut sum = 0;
    let mut sum2 = 0;
    while let Some(line) = lines.next() {
        sum += matches(&productions, vec![0], &line, 0) as usize;
        sum2 += matches(&productions2, vec![0], &line, 0) as usize;
    }
    println!("{} {}", sum, sum2);

    Ok(())
}

fn matches(
    productions: &HashMap<usize, RuleOutput>,
    mut rule_list: Vec<usize>,
    line: &str,
    index: usize,
) -> bool {
    if rule_list.is_empty() {
        return index == line.len();
    }

    let rule = rule_list.remove(0);

    match &productions[&rule] {
        Terminal(c) => {
            if index < line.len() && line.as_bytes()[index] == *c as u8 {
                matches(productions, rule_list, line, index + 1)
            } else {
                false
            }
        }
        NonTerminal(union_products) => {
            for product in union_products {
                let mut new_rule_list = product.clone();
                new_rule_list.append(&mut rule_list.clone());

                if matches(productions, new_rule_list, line, index) {
                    return true;
                }
            }

            false
        }
    }
}
