use std::collections::*;
use std::fs::*;
use std::io::*;

fn main() -> anyhow::Result<()> {
    let input = File::open("day10.in")?;
    let input: BTreeSet<_> = BufReader::new(input)
        .lines()
        .map(|s| s.unwrap().parse::<usize>().unwrap())
        .collect();

    // Part A.
    let mut prev = 0;
    let mut total1 = 0;
    let mut total3 = 1;

    for &joltage in &input {
        match joltage - prev {
            1 => {
                total1 += 1;
            }
            3 => {
                total3 += 1;
            }
            _ => panic!(),
        }
        prev = joltage;
    }

    println!("{}", total1 * total3);

    // Part B.
    let mut arrangements_up_to = BTreeMap::<usize, usize>::new();
    arrangements_up_to.insert(0, 1);

    for &joltage in &input {
        let mut arrangements = 0;

        let prev_joltages = arrangements_up_to.range(joltage.checked_sub(3).unwrap_or(0)..joltage);
        for (_, prev_arrangements) in prev_joltages {
            arrangements += prev_arrangements;
        }

        arrangements_up_to.insert(joltage, arrangements);
    }

    let last_num_arrangements = arrangements_up_to.into_iter().rev().next().unwrap().1;
    println!("{}", last_num_arrangements);

    Ok(())
}
