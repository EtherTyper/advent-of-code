use aoc::intcode;
use aoc::util::*;
use std::collections::*;
use std::iter::*;
use Instruction::*;

#[derive(Copy, Clone, Debug, PartialEq)]
enum Instruction {
    Forward(isize),
    Left,
    Right,
}

fn main() -> anyhow::Result<()> {
    let initial_state = intcode::read_tape("2019day17.in")?;
    let (output, _) = initial_state.clone().eval(empty::<isize>());

    let mut start = None;
    let mut set = HashSet::new();

    let mut row = 0;
    let mut col = 0;

    for c in output {
        let c = c as u8 as char;
        match c {
            '#' => {
                set.insert((row, col));
                col += 1;
            }
            '\n' => {
                row += 1;
                col = 0;
            }
            '^' => {
                start = Some((row, col));
                col += 1;
            }
            _ => col += 1,
        }
        // print!("{}", c);
    }

    let mut sum_alignment = 0;
    for &(row, col) in &set {
        if set.contains(&(row + 1, col))
            && set.contains(&(row - 1, col))
            && set.contains(&(row, col + 1))
            && set.contains(&(row, col - 1))
        {
            sum_alignment += row * col;
        }
    }

    println!("{}", sum_alignment);

    // Part B.
    if false {
        if let Some(mut point) = start {
            // Trace path, find raw instructions to follow it.
            let mut current_direction = 0;
            let mut instructions = vec![];
            loop {
                let directions = vec![
                    (point.0 - 1, point.1),
                    (point.0, point.1 + 1),
                    (point.0 + 1, point.1),
                    (point.0, point.1 - 1),
                ];
                let front = directions[modulo(current_direction, 4) as usize];
                let left = directions[modulo(current_direction - 1, 4) as usize];
                let right = directions[modulo(current_direction + 1, 4) as usize];
                if set.contains(&front) {
                    if let Some(&Forward(distance)) = instructions.last() {
                        instructions.pop();
                        instructions.push(Forward(distance + 1));
                    } else {
                        instructions.push(Forward(1));
                    }
                    point = front;
                } else if set.contains(&left) {
                    instructions.push(Left);
                    current_direction -= 1;
                } else if set.contains(&right) {
                    instructions.push(Right);
                    current_direction += 1;
                } else {
                    break;
                }
            }

            // Display string of instructions.
            for instruction in instructions {
                print!(
                    "{},",
                    match instruction {
                        Forward(val) => format!("{}", val),
                        Left => String::from("L"),
                        Right => String::from("R"),
                    }
                );
            }
            println!();
        }
    }

    // Reduced manually from the above.
    // MUST be replaced for other data sets.
    let script = r#"
B,A,B,C,B,B,C,A,C,A
L,10,L,8,L,12,R,12
L,12,L,8,R,12
R,12,L,8,L,10
n
    "#
    .trim_start();

    let mut new_state = initial_state;
    new_state.tape[0] = 2;

    let (output_bytes, _) = new_state.eval(script.bytes().map(|i| i as isize));
    for x in output_bytes {
        if x > u8::MAX as isize {
            println!("{}", x);
        } else {
            // print!("{}", x as u8 as char);
        }
    }

    Ok(())
}

#[allow(unused)]
fn longest_repeated_subsequence<T>(vector: &Vec<T>) -> &[T]
where
    T: PartialEq,
{
    let mut max_slice = &vector[0..0];
    let mut max_offset = 0;
    for offset in 1..vector.len() {
        let mut last_reset = 0;
        for index in 0..vector.len() - offset {
            if vector[index] != vector[index + offset] {
                last_reset = index + 1;
            } else {
                if index + 1 - last_reset > max_slice.len() {
                    max_offset = offset;
                    max_slice = &vector[last_reset..=index];
                }
            }
        }
    }

    println!("max offset {}", max_offset);
    max_slice
}
