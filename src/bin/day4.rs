use scanner_rust::*;
use std::collections::*;
use std::fs::*;
use std::io::*;

fn main() -> anyhow::Result<()> {
    let input = File::open("day4.in")?;
    let mut lines = BufReader::new(input).lines().peekable();

    let required_fields: Vec<_> = vec!["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
        .iter()
        .map(|&i| String::from(i))
        .collect();
    let hair_colors: Vec<_> = vec!["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
        .iter()
        .map(|&i| String::from(i))
        .collect();

    let mut present_count = 0;
    let mut valid_count = 0;

    'outer: while let Some(_) = lines.peek() {
        let mut fields = HashMap::<String, String>::new();

        while let Some(Ok(line)) = lines.next() {
            if line.is_empty() {
                break;
            }

            let mut line_scanner = Scanner::new(line.as_bytes());

            while !line_scanner.peek(true)?.is_empty() {
                let field = line_scanner.next_until(":")?.unwrap();
                let value = line_scanner.next()?.unwrap();
                line_scanner.skip_whitespaces()?;

                fields.insert(field, value);
            }
        }

        if required_fields.iter().all(|i| fields.contains_key(i)) {
            present_count += 1;

            // byr
            if fields["byr"].len() != 4 {
                continue;
            }
            if let Ok(byr) = fields["byr"].parse::<usize>() {
                if !(1920 <= byr && byr <= 2002) {
                    continue;
                }
            } else {
                continue;
            }
            // iyr
            if fields["iyr"].len() != 4 {
                continue;
            }
            if let Ok(byr) = fields["iyr"].parse::<usize>() {
                if !(2010 <= byr && byr <= 2020) {
                    continue;
                }
            } else {
                continue;
            }
            // eyr
            if fields["eyr"].len() != 4 {
                continue;
            }
            if let Ok(byr) = fields["eyr"].parse::<usize>() {
                if !(2020 <= byr && byr <= 2030) {
                    continue;
                }
            } else {
                continue;
            }
            // hgt
            if fields["hgt"].len() < 2 {
                continue;
            }
            let hgt_unit = &fields["hgt"][(fields["hgt"].len() - 2)..(fields["hgt"].len())];
            if let Ok(hgt) = fields["hgt"][0..(fields["hgt"].len() - 2)].parse::<usize>() {
                match hgt_unit {
                    "cm" => {
                        if !(150 <= hgt && hgt <= 193) {
                            continue;
                        }
                    }
                    "in" => {
                        if !(59 <= hgt && hgt <= 76) {
                            continue;
                        }
                    }
                    _ => {
                        continue;
                    }
                }
            } else {
                continue;
            }
            // hcl
            if fields["hcl"].len() != 7 || &fields["hcl"][0..1] != "#" {
                continue;
            }
            for c in fields["hcl"][1..].chars() {
                if !('0' <= c && c <= '9' || 'a' <= c && c <= 'f') {
                    continue 'outer;
                }
            }
            // ecl
            if !hair_colors.contains(&fields["ecl"]) {
                continue;
            }
            // pid
            if fields["pid"].len() != 9 {
                continue;
            }
            if fields["pid"].parse::<usize>().is_err() {
                continue;
            }

            valid_count += 1;
        }
    }

    println!("{}", present_count);
    println!("{}", valid_count);

    Ok(())
}
