use aoc::intcode;
use std::convert::*;
use std::env::*;
use std::io::*;

// For 2019 day 21 and 25.
fn main() -> anyhow::Result<()> {
    let mut state = intcode::read_tape(&args_os().nth(1).unwrap().into_string().unwrap())?;
    let mut input_bytes: Vec<isize> = vec![];

    loop {
        let (output_bytes, complete) = state.eval(input_bytes);
        let output = String::from_utf8(
            output_bytes
                .into_iter()
                .map(|x| match u8::try_from(x) {
                    Ok(val) => val,
                    Err(_) => 0,
                })
                .collect::<Vec<_>>(),
        )?;

        print!("{}", output);
        if complete {
            break;
        }

        let mut input = String::new();
        stdin().read_line(&mut input)?;
        input_bytes = input.into_bytes().into_iter().map(isize::from).collect();
    }

    Ok(())
}
