use std::collections::*;
use std::fs::*;
use std::io::*;

fn main() -> anyhow::Result<()> {
    let input = File::open("day6.in")?;
    let mut lines = BufReader::new(input).lines().peekable();
    let mut some_total = 0;
    let mut all_total = 0;

    while let Some(_) = lines.peek() {
        let mut letter_incidence = HashMap::<char, usize>::new();
        let mut people = 0;

        while let Some(Ok(line)) = lines.next() {
            if line.is_empty() {
                break;
            }

            for letter in line.chars() {
                let &incidence = letter_incidence.get(&letter).unwrap_or(&0);
                letter_incidence.insert(letter, incidence + 1);
            }
            people = people + 1;
        }

        some_total += letter_incidence.len();
        all_total += letter_incidence
            .into_iter()
            .filter(|&(_, v)| v == people)
            .count();
    }

    println!("{}", some_total);
    println!("{}", all_total);

    Ok(())
}
