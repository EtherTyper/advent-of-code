use aoc::intcode;
use itertools::Itertools;

fn main() -> anyhow::Result<()> {
    let initial_state = intcode::read_tape("2019day7.in")?;
    let amplifiers = 5;

    let max_output = (0..amplifiers as isize)
        .permutations(amplifiers)
        .map(|order| {
            let mut input = 0;
            for phase_setting in order {
                let (output, _) = initial_state.clone().eval(vec![phase_setting, input]);

                input = output[0];
            }
            input
        })
        .max()
        .unwrap();

    println!("{}", max_output);

    let max_feedback_output = (amplifiers as isize..(2 * amplifiers) as isize)
        .permutations(amplifiers)
        .map(|order| {
            // Initialize amplifiers and supply phase settings.
            let mut states: Vec<intcode::ComputerState> = Vec::with_capacity(amplifiers);
            for (i, &phase_setting) in order.iter().enumerate() {
                states.push(initial_state.clone());
                states[i].eval(vec![phase_setting]);
            }

            // Supply inputs in a loop and evaluate.
            let mut input = 0;
            'outer: loop {
                for i in 0..amplifiers {
                    let (output, complete) = states[i].eval(vec![input]);

                    input = output[0];
                    if complete && i == amplifiers - 1 {
                        break 'outer;
                    }
                }
            }

            input
        })
        .max()
        .unwrap();

    println!("{}", max_feedback_output);

    Ok(())
}
