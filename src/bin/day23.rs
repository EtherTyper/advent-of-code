use std::cmp::*;
use std::collections::*;
use std::fs::*;

fn main() -> anyhow::Result<()> {
    let input: Vec<usize> = read_to_string("day23.in")?
        .trim()
        .chars()
        .map(|x| x.to_digit(10).unwrap() as usize)
        .collect();

    let (link0, result) = play_game(input.clone(), 100);
    let mut link = result[link0].1;
    while link != link0 {
        let (value, new_link) = result[link];
        link = new_link;

        print!("{}", value);
    }
    println!();

    let mut input2 = input.clone();
    input2.reserve(1_000_000);
    for i in input.len()..1_000_000 {
        input2.push(i + 1);
    }

    let (link0, result) = play_game(input2, 10_000_000);
    let link1 = result[link0].1;
    let (value1, link2) = result[link1];
    let (value2, _) = result[link2];
    println!("{}", value1 * value2);

    Ok(())
}

fn play_game(integers: Vec<usize>, steps: usize) -> (usize, Vec<(usize, usize)>) {
    // Linked list. We must implement this manually because we need to
    // store references to individual nodes.
    let len = integers.len();
    let mut integers: Vec<(usize, usize)> = integers
        .into_iter()
        .enumerate()
        .map(|(index, value)| (value, (index + 1) % len))
        .collect();

    let mut location_of = HashMap::<usize, usize>::new();
    for index in 0..min(len, 10) {
        let (value, _) = integers[index];
        location_of.insert(value, index);
    }

    let &(maximum, _) = integers.iter().max_by_key(|&(val, _)| val).unwrap();

    let mut link_current = 0;
    for _ in 0..steps {
        let (current, link1) = integers[link_current];
        let (val1, link2) = integers[link1];
        let (val2, link3) = integers[link2];
        let (val3, link_after_val3) = integers[link3];
        integers[link_current].1 = link_after_val3;

        let mut destination_num = current;
        loop {
            if destination_num == 0 {
                destination_num = maximum;
            } else if destination_num == current
                || destination_num == val1
                || destination_num == val2
                || destination_num == val3
            {
                destination_num -= 1;
            } else {
                break;
            }
        }

        // NOTE: This optimization assumes integers[i] = i + 1 for large i (i >= 10.)
        // True for the two applications of the game we were given.
        let link_destination = if destination_num < 10 {
            location_of[&destination_num]
        } else {
            destination_num - 1
        };
        let (_, link_after_destination) = integers[link_destination];
        integers[link_destination].1 = link1;
        integers[link3].1 = link_after_destination;

        link_current = integers[link_current].1;
    }

    (location_of[&1], integers)
}
