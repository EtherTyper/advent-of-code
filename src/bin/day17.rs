use itertools::*;
use std::cmp;
use std::fs::*;
use std::io::*;

fn main() -> anyhow::Result<()> {
    let input = File::open("day17.in")?;
    let initial_grid: Vec<Vec<char>> = BufReader::new(input)
        .lines()
        .map(|s| s.unwrap().chars().collect())
        .collect();

    // Part A.
    automoton3d(
        6,
        &vec![initial_grid.clone()],
        |preimage, (dim_x, dim_y, dim_z), (x, y, z)| {
            let mut neighbors_occupied = 0;
            for (neighbor_x, neighbor_y, neighbor_z) in iproduct!(
                x.checked_sub(2).unwrap_or(0)..cmp::min(x + 1, dim_x),
                y.checked_sub(2).unwrap_or(0)..cmp::min(y + 1, dim_y),
                z.checked_sub(2).unwrap_or(0)..cmp::min(z + 1, dim_z)
            ) {
                if (neighbor_x + 1, neighbor_y + 1, neighbor_z + 1) == (x, y, z) {
                    continue;
                }
                neighbors_occupied +=
                    (preimage[neighbor_x][neighbor_y][neighbor_z] == '#') as usize;
            }

            let existing_value =
                if x > 0 && x - 1 < dim_x && y > 0 && y - 1 < dim_y && z > 0 && z - 1 < dim_z {
                    preimage[x - 1][y - 1][z - 1]
                } else {
                    '.'
                };

            match existing_value {
                '#' => {
                    if neighbors_occupied == 2 || neighbors_occupied == 3 {
                        '#'
                    } else {
                        '.'
                    }
                }
                '.' => {
                    if neighbors_occupied == 3 {
                        '#'
                    } else {
                        '.'
                    }
                }
                _ => panic!(),
            }
        },
    );

    // Part B
    automoton4d(
        6,
        &vec![vec![initial_grid]],
        |preimage, (dim_x, dim_y, dim_z, dim_w), (x, y, z, w)| {
            let mut neighbors_occupied = 0;
            for (neighbor_x, neighbor_y, neighbor_z, neighbor_w) in iproduct!(
                x.checked_sub(2).unwrap_or(0)..cmp::min(x + 1, dim_x),
                y.checked_sub(2).unwrap_or(0)..cmp::min(y + 1, dim_y),
                z.checked_sub(2).unwrap_or(0)..cmp::min(z + 1, dim_z),
                w.checked_sub(2).unwrap_or(0)..cmp::min(w + 1, dim_w)
            ) {
                if (
                    neighbor_x + 1,
                    neighbor_y + 1,
                    neighbor_z + 1,
                    neighbor_w + 1,
                ) == (x, y, z, w)
                {
                    continue;
                }
                neighbors_occupied +=
                    (preimage[neighbor_x][neighbor_y][neighbor_z][neighbor_w] == '#') as usize;
            }

            let existing_value = if x > 0
                && x - 1 < dim_x
                && y > 0
                && y - 1 < dim_y
                && z > 0
                && z - 1 < dim_z
                && w > 0
                && w - 1 < dim_w
            {
                preimage[x - 1][y - 1][z - 1][w - 1]
            } else {
                '.'
            };

            match existing_value {
                '#' => {
                    if neighbors_occupied == 2 || neighbors_occupied == 3 {
                        '#'
                    } else {
                        '.'
                    }
                }
                '.' => {
                    if neighbors_occupied == 3 {
                        '#'
                    } else {
                        '.'
                    }
                }
                _ => panic!(),
            }
        },
    );

    Ok(())
}

fn automoton3d<K>(passes: usize, initial_grid: &Vec<Vec<Vec<char>>>, closure: K)
where
    K: Fn(&Vec<Vec<Vec<char>>>, (usize, usize, usize), (usize, usize, usize)) -> char,
{
    let mut preimage = initial_grid.clone();
    let mut image: Vec<Vec<Vec<char>>>;
    let mut dimensions = (
        initial_grid.len(),
        initial_grid[0].len(),
        initial_grid[0][0].len(),
    );

    for _ in 0..passes {
        image = Vec::with_capacity(preimage.len() + 2);

        for x in 0..dimensions.0 + 2 {
            image.push(Vec::with_capacity(dimensions.0 + 2));
            for y in 0..dimensions.1 + 2 {
                image[x].push(Vec::with_capacity(dimensions.1 + 2));
                for z in 0..dimensions.2 + 2 {
                    image[x][y].push(closure(&preimage, dimensions, (x, y, z)));
                }
            }
        }

        preimage = image;

        dimensions.0 += 2;
        dimensions.1 += 2;
        dimensions.2 += 2;
    }

    println!(
        "{}",
        preimage
            .into_iter()
            .map(|x| x
                .into_iter()
                .map(|y| y.into_iter().filter(|&z| z == '#').count())
                .sum::<usize>())
            .sum::<usize>()
    );
}

fn automoton4d<K>(passes: usize, initial_grid: &Vec<Vec<Vec<Vec<char>>>>, closure: K)
where
    K: Fn(
        &Vec<Vec<Vec<Vec<char>>>>,
        (usize, usize, usize, usize),
        (usize, usize, usize, usize),
    ) -> char,
{
    let mut preimage = initial_grid.clone();
    let mut image: Vec<Vec<Vec<Vec<char>>>>;
    let mut dimensions = (
        initial_grid.len(),
        initial_grid[0].len(),
        initial_grid[0][0].len(),
        initial_grid[0][0][0].len(),
    );

    for _ in 0..passes {
        image = Vec::with_capacity(preimage.len() + 2);

        for x in 0..dimensions.0 + 2 {
            image.push(Vec::with_capacity(dimensions.0 + 2));
            for y in 0..dimensions.1 + 2 {
                image[x].push(Vec::with_capacity(dimensions.1 + 2));
                for z in 0..dimensions.2 + 2 {
                    image[x][y].push(Vec::with_capacity(dimensions.2 + 2));
                    for w in 0..dimensions.3 + 2 {
                        image[x][y][z].push(closure(&preimage, dimensions, (x, y, z, w)));
                    }
                }
            }
        }

        preimage = image;

        dimensions.0 += 2;
        dimensions.1 += 2;
        dimensions.2 += 2;
        dimensions.3 += 2;
    }

    println!(
        "{}",
        preimage
            .into_iter()
            .map(|x| x
                .into_iter()
                .map(|y| y
                    .into_iter()
                    .map(|z| z.into_iter().filter(|&z| z == '#').count())
                    .sum::<usize>())
                .sum::<usize>())
            .sum::<usize>()
    );
}
