use aoc::intcode;
use std::convert::*;
use string_builder::*;

fn main() -> anyhow::Result<()> {
    let mut common_state = intcode::read_tape("2019day25.in")?;
    // List of items found in path. MUST be replaced for other data sets.
    let items = vec![
        "weather machine",
        "polygon",
        "fixed point",
        "astronaut ice cream",
        "easter egg",
        "dark matter",
        "food ration",
        "asterisk",
    ];

    // Path taking all items and ending at Security Checkpoint.
    // MUST be replaced for other data sets.
    let input_bytes: Vec<isize> = r#" 
west
south
south
south
take asterisk
north
north
north
west
south
take astronaut ice cream
south
take polygon
east
take easter egg
west
north
north
west
south
take fixed point
west
take food ration
east
north
west
take dark matter
east
east
south
south
east
east
take weather machine
north
"#
    .trim_start()
    .as_bytes()
    .iter()
    .map(|&x| x as isize)
    .collect();

    let (output_bytes, _) = common_state.eval(input_bytes);
    let output = String::from_utf8(
        output_bytes
            .into_iter()
            .map(|x| match u8::try_from(x) {
                Ok(val) => val,
                Err(_) => panic!(),
            })
            .collect::<Vec<_>>(),
    )?;
    print!("{}", output);

    // Loop over bitstrings representing subsets of these items to drop.
    for i in 0..(2 << items.len()) {
        let mut state = common_state.clone();
        let mut builder = Builder::default();
        for k in 0..items.len() {
            if (i >> k) & 1 != 0 {
                builder.append(format!("drop {}\n", items[k]));
            }
        }
        let input_bytes: Vec<isize> = format!(
            r#"
{}
inv
north
"#,
            builder.string()?.trim_end()
        )
        .trim_start()
        .as_bytes()
        .iter()
        .map(|&x| x as isize)
        .collect();

        let (output_bytes, _) = state.eval(input_bytes);
        let output = String::from_utf8(
            output_bytes
                .into_iter()
                .map(|x| match u8::try_from(x) {
                    Ok(val) => val,
                    Err(_) => panic!(),
                })
                .collect::<Vec<_>>(),
        )?;

        if !output.contains("lighter") && !output.contains("heavier") {
            print!("{}", output);
            break;
        }
    }

    Ok(())
}
