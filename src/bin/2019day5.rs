use aoc::intcode;

fn main() -> anyhow::Result<()> {
    let initial_state = intcode::read_tape("2019day5.in")?;

    let (output, _) = initial_state.clone().eval(vec![1 as isize]);
    for value in output {
        println!("{}", value);
    }

    println!("---");

    let mut state = initial_state;
    let (output, _) = state.eval(vec![5 as isize]);
    for value in output {
        println!("{}", value);
    }

    Ok(())
}
