use aoc::intcode;

fn main() -> anyhow::Result<()> {
    let initial_state = intcode::read_tape("2019day21.in")?;
    let script1 = r#"
        NOT A J
        NOT B T
        OR T J
        NOT C T
        OR T J
        AND D J
        WALK
    "#
    .trim_start();
    let script2 = r#"
        NOT A J
        NOT B T
        OR T J
        NOT C T
        OR T J
        AND D J
        NOT H T
        NOT T T
        OR E T
        AND T J
        RUN
    "#
    .trim_start();

    for script in vec![script1, script2] {
        let (output_bytes, _) = initial_state
            .clone()
            .eval(script.bytes().map(|i| i as isize));
        for x in output_bytes {
            if x > u8::MAX as isize {
                println!("{}", x);
            } else {
                // print!("{}", x as u8 as char);
            }
        }
    }

    Ok(())
}
