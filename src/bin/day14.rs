use scanner_rust::*;
use std::collections::*;
use std::fs::*;
use std::io::*;

fn main() -> anyhow::Result<()> {
    let input = File::open("day14.in")?;
    let lines = BufReader::new(input).lines();

    let mut mask = None;
    let mut map = HashMap::<usize, usize>::new();

    for line in lines {
        let line = line?;

        match &line[0..3] {
            "mas" => {
                let string_mask = &line[7..];
                let and_mask = usize::from_str_radix(&string_mask.replace("X", "1"), 2)?;
                let or_mask = usize::from_str_radix(&string_mask.replace("X", "0"), 2)?;
                mask = Some((and_mask, or_mask));
            }
            "mem" => {
                let mut scanner = ScannerStr::new(&line);
                for _ in 0..4 {
                    scanner.next_char()?.unwrap();
                }
                let key = scanner.next_usize_until("]")?.unwrap();
                for _ in 0..3 {
                    scanner.next_char()?.unwrap();
                }
                let value = scanner.next_usize()?.unwrap();

                let (and_mask, or_mask) = mask.unwrap();
                let masked_value = value & and_mask | or_mask;
                map.insert(key, masked_value);
            }
            _ => {}
        }
    }

    println!("{}", map.values().sum::<usize>());

    let input = File::open("day14.in")?;
    let lines = BufReader::new(input).lines();

    let mut mask = None;
    let mut map = HashMap::<usize, usize>::new();

    for line in lines {
        let line = line?;

        match &line[0..3] {
            "mas" => {
                let string_mask = &line[7..];
                let or_mask = usize::from_str_radix(&string_mask.replace("X", "0"), 2)?;
                mask = Some((String::from(string_mask), or_mask));
            }
            "mem" => {
                let mut scanner = ScannerStr::new(&line);
                for _ in 0..4 {
                    scanner.next_char()?.unwrap();
                }
                let key = scanner.next_usize_until("]")?.unwrap();
                for _ in 0..3 {
                    scanner.next_char()?.unwrap();
                }
                let value = scanner.next_usize()?.unwrap();

                let (string_mask, or_mask) = mask.clone().unwrap();
                let mut masked_keys = vec![key | or_mask];
                for i in 0..string_mask.len() {
                    if string_mask.as_bytes()[i] == 'X' as u8 {
                        masked_keys = masked_keys
                            .into_iter()
                            .flat_map(|key| vec![key, key ^ (1 << (35 - i))])
                            .collect();
                    }
                }
                for masked_key in masked_keys {
                    map.insert(masked_key, value);
                }
            }
            _ => {}
        }
    }

    println!("{}", map.values().sum::<usize>());

    Ok(())
}
