use aoc::intcode;
use std::mem::*;

fn main() -> anyhow::Result<()> {
    let machines = 50;

    let initial_state = intcode::read_tape("2019day23.in")?;
    let mut states = Vec::with_capacity(machines);
    let mut incoming_packets = Vec::with_capacity(machines);
    let mut complete = vec![false; machines];
    let mut nat = None;
    let mut dry_streak = 0;

    let mut last_y = None;

    for i in 0..machines {
        states.push(initial_state.clone());
        incoming_packets.push(vec![i as isize]);
    }

    loop {
        let mut packets_sent = false;

        for i in 0..machines {
            if complete[i] {
                continue;
            }

            let mut incoming = vec![-1];
            swap(&mut incoming_packets[i], &mut incoming);
            let (output, is_complete) = states[i].eval(incoming);

            complete[i] = is_complete;

            let mut output_iter = output.into_iter();
            while let Some(target) = output_iter.next() {
                packets_sent = true;
                let x = output_iter.next().unwrap();
                let y = output_iter.next().unwrap();
                if target == 255 {
                    if nat == None {
                        println!("{}", y);
                    }
                    nat = Some((x, y));
                } else {
                    let target_packets = &mut incoming_packets[target as usize];
                    target_packets.push(x);
                    target_packets.push(y);
                }
            }
        }

        if !packets_sent {
            dry_streak += 1;
        } else {
            dry_streak = 0;
        }

        if dry_streak >= 2 {
            let (x, y) = nat.unwrap();

            if Some(y) == last_y {
                println!("{}", y);
                break;
            }
            last_y = Some(y);

            let address_zero_packets = &mut incoming_packets[0];
            address_zero_packets.push(x);
            address_zero_packets.push(y);
        }
    }

    Ok(())
}
